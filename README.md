# OMEKA-S (multisite) con docker

Versión de omeka-S usando recetas dockercompose.yml y Dockerfiles.

Está dividido en tres containers: 
* #### *nginx*
* #### *php-fpm*
* #### *mariadb*

<br>

#### ***nginx***
El servidor que ofrece la web.  

* Expone el puerto **8086** externamente (docker crea la regla de firewall en el host) y lo redirige al 8086 interno.  
* Volúmenes:  
    * `omeka-s_data`: volumen manejado por docker, almacena la instalación de Omeka-S (el árbol de directorios de la aplicación php).  
    * `./omeka_data/files`: volumen local, uploads y thumbnails.   
    * `./omeka_data/config`: volumen local, donde podemos configurar la base de datos a utilizar y otros parámetros de la aplicación.  
    * `./omeka_data/themes`: volumen local, carpeta themes de la instalación.  
    * `./omeka_data/modules`: volumen local, carpeta modules de la instalación.  

Los 3 últimos nos permitirán hacer cambios a la configuración y añadir temas o plugins sin reiniciar el contenedor.  

En la carpeta `./omeka_data/files/` omeka escribe los ficheros subidos,crear los thumbnails, ... La sacamos fuera para facilitar una migración (hacia o desde nuestra insalación). Cambiaremos los permisos de esa carpeta para que nuestra aplicación pueda escribir allí.  
 
#### ***php-fpm***  
El intérprete de php, traducirá nuestro php a html.  

* Expone el puerto **9000** al resto de contenedores, para las peticiones de fastcgi desde el container *nginx*.   
Volúmenes:  
    * Aceso a los mismos volúmenes de datos que el contenedor *nginx*.   

#### ***mariadb***
Base de datos con volumen propio.  
* Expone el puerto **3306** al resto de contenedores.  
* Volúmenes:  
    * omeka_db: volumen manejado por docker, archivos de la base de datos MariaDB.  
<br>

Los dos primeros contenedores arrancarán su proceso bajo el `uid` y `gid` `2000` (denifido en sus Dockerfiles), que en los contenedores se identifica con el usuario **omeka**. Ese usuario tendrá acceso a los datos en los volúmenes creados desde ambos contenedores.  

El contenedor *mariadb* corre como `uid` y `gid` `2001`.


#### Opcional
Los usuarios omeka y omekadb, en cualquier caso, son creados por docker al vuelo para arrancar los procesos (*nginx*, *php-fpm*, *mariadb*), no es necesario que existan en el host. 
No obstante, en producción y para evitar colisiones de uid o gid no está de más que crees estos usuarios antes de empezar.  
``` 
useradd -r -d /opt/omeka -s /usr/sbin/nologin omeka
id omeka
``` 
Eso creará un usuario omeka con home en /opt/omeka, sin shell, y mostrará el `uid y `gid` de sistema asignados (< 1000 en debian). La carpeta del usuario no se crea, la crearemos después. 
Repite lo mismo para el usuario omekadb. Y recuerda cambiar los valores númerico de usuario y grupo también en ambos contenedores (Dockerfiles).   

> NOTA: las colisiones de uid y gid pueden suponer un problema de seguridad. Si el proceso escapa de su contenedor actuará con los permisos de acceso, lectura o escritura de ese uid/gid en el sistema de ficheros del host. Y podría acceder a ficheros o directorios de otro contenedor o ficheros del sistema. 
    

### Instalación

*Todos los comandos como root.*

1. Clona el repo en la carpeta donde vayas a alojar tu docker. La carpeta creada será la home del usuario omeka si has optado por crearlo antes, pero no es necesario que sea propiedad de ese usuario.
    ``` 
    git clone https://git.sindominio.net/pebles/omeka-s.git /opt/omeka-s
    ``` 
2. Entra y edita el fichero `.env` para que contenga el nombre del directorio en el volumen compartido donde se instalará el árbol de directorios de Omeka-S. 

3. Configuración de omeka en `./omeka_data/config`:
    * Renombra el fichero `database.ini.example` a `database.ini` y rellénalo con los datos de la db. Los datos que pongas lo utilizarás después al crear la base de datos.  
    * Renombra el fichero `local.config.php.example` quitando *example*.  
    En las primeras líneas de ese fichero también puedes activar el log de la aplicación, que por defecto escupirá a `./logs/`.   
    Edita el bloque final con tu dominio tal cómo es visible desde el exterior. Esto hará que se sobreescriban la urls de las imágenes y otros recursos si estás detrás de un proxy.  
        ``` 
        'file_store' => [
            'local' => [
                   'base_uri' => 'https://mydomain.net/files',
            ],
        ],
        ```
4. Cambia propietario y grupo de la carpeta `./omeka_data/files` al `uid` y `gid` elegido:  
    ```
    chown -R 2000:2000 omeka_data/files
    ```

5. Construye e instancia una imagen (lanza un contenedor):  
    ```
    docker-compose build 
    docker-compose up -d
    ```
     Tu servidor ya es accesible en tu puerto 8086. Pero **no funciona**, todavía no tienes base de datos.

6. Entra en el contenedor *mariadb* como root:  
    ```
    docker exec -it --user root omeka_db mysql 
    ```
    Y crea la base de datos y su usuario:
    ```
    CREATE DATABASE omeka CHARACTER SET utf8 COLLATE utf8_general_ci;
    GRANT ALL ON omeka.* TO 'omeka'@'%' IDENTIFIED BY 'mysecretstring';
    FLUSH PRIVILEGES;
    QUIT
    ```
    Finalmente, por seguridad, borra el historial de mysql antes de salir del contenedor:
    ```
    rm /root/.mysql_history
    exit
    ```

<br>

Ya puedes refrescar tu navegador. Ahora deberías ver la página de instalación. 

Una vez instalado los datos serán persistentes. Puedes reiniciar los contenedores con un `docker-compose restart`, o recrear la imagen con `build` si fuera necesario.

### Temas y módulos
En el directorio `./omeka_data` en tu clon del repo puedes incorporar nuevos temas y módulos (descomprimidos) para acabar de instalarlos en tu panel admin en la web.
El la carpeta *themes* además del tema por defecto he añadido el tema **foundation**, y en la carpeta modules **OAI-PMH-Repository**.

### Actualizaciones.
El repositorio está preparado para descargar la versión 2.1.2 de omeka-s, que no es la última en este momento pero que es compatible con el módulo OAI-PMH-Repository, y nos interesa.
1. Edita `./php-fpm/Dockerfile` y cambia la línea de descarga del software para la versión deseada.
2. Elimina el volumen donde se alberga el php (manejado por docker) y reconstruye la imagen para insertar la nueva versión. (Tu base de datos, tus ficheros subidos, tu configuración y tus temas y módulos están a salvo en otros volúmenes).
    ```
    docker volume ls
    docker volume rm omeka_data 
    docker-compose build
    docker-compose up -d
    ```
3. La versión 3 utiliza el framework Laminas en vez de Zend (hasta ver2), por lo que deberás sustituir el fichero local.config.php con el contenido de local.config.php.omeka-v3, o el que se ofrece en la web de omeka.

Tras la actualización sigue las indicaciones en la parte admin de la web, si las hubiera (botón para actualizar la base de datos).


### Migración
Si tienes un omeka-s (multisite) anterior, puedes migrar su base de datos a tu nueva instalación:
```
 $ docker exec -it --user root omeka_db mysql omeka < database_vieja.sql

```
Y copiar las carpetas files, themes y modules a ./omeka_data/files.


### TODO
Manage logs.



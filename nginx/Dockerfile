FROM debian:buster-slim

ENV USER_ID 2000
ENV GROUP_ID 2000
ARG BASE_DIR

RUN apt-get update && \
    apt-get -qy install nginx-full && \
    apt-get clean

RUN groupadd -g $GROUP_ID omeka && \
    useradd -m -d $BASE_DIR -s  /bin/sh -g $GROUP_ID -u $USER_ID omeka

COPY --chown=$USER_ID:$GROUP_ID site.conf /etc/nginx/sites-available/site.conf
COPY --chown=$USER_ID:$GROUP_ID fastcgi_params /etc/nginx/fastcgi_params

RUN sed -i "s|\/sindominio|$BASE_DIR|g" /etc/nginx/sites-available/site.conf && \
    rm /etc/nginx/sites-enabled/default && \
    ln -s /etc/nginx/sites-available/site.conf /etc/nginx/sites-enabled/site.conf && \
    # disable user on nginx.conf first line so we run it with non-root user (avoid  warnings)
    sed -i ' 1 s/\(.*\)/#\1/' /etc/nginx/nginx.conf && \ 
    chown -R $USER_ID:$GROUP_ID /var/log/nginx && \
    chown -R $USER_ID:$GROUP_ID /var/lib/nginx && \
    touch /run/nginx.pid && \
    chown $USER_ID:$GROUP_ID /run/nginx.pid

CMD /usr/sbin/nginx -g "daemon off; master_process off;"

